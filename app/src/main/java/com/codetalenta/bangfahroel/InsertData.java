package com.codetalenta.bangfahroel;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.AuthFailureError;
import com.android.volley.Network;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public class InsertData extends AppCompatActivity {
    EditText editTextName, editTextEmail;
    Button submit;
    Session session;
    private final String url = Server.URL_BANG_FAHROEL + "savemahasiswa";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insert_data);
        editTextName = findViewById(R.id.name);
        editTextEmail = findViewById(R.id.email);
        session = new Session(this);
        submit = findViewById(R.id.submit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                volleyPost();
            }
        });
    }

    /*
    * method insert data
    * */
    private void volleyPost() {
        final String name = editTextName.getText().toString();
        final String email = editTextEmail.getText().toString();
        JSONObject postData = new JSONObject();
        try {
            postData.put("nama", name);
            postData.put("email", email);
        } catch (JSONException e) {

        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, postData, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                startActivity(new Intent(InsertData.this, MainActivity.class));
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }


        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                String token = session.getString("token");
                params.put("Authorization", token);
                return params;
            }
        };

        AppController.getInstance().addToRequestQueue(jsonObjectRequest);
    }


}