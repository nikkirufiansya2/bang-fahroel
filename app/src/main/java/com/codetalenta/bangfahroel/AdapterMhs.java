package com.codetalenta.bangfahroel;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class AdapterMhs extends RecyclerView.Adapter<AdapterMhs.MyView> {
    List<MhsModel> data;
    Context context;

    public AdapterMhs(Context context, List<MhsModel> data){
        this.context = context;
        this.data = data;
    }

    @NonNull
    @Override
    public AdapterMhs.MyView onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
        return new MyView(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterMhs.MyView holder, int position) {
        holder.nama.setText(data.get(position).getNama());
        holder.email.setText(data.get(position).getEmail());
        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, EditData.class);
                /*
                * send data to Activity EditData
                * */
                int id = data.get(position).getId();
                String nama = data.get(position).getNama();
                String email =  data.get(position).getEmail();
                intent.putExtra("id", id);
                intent.putExtra("nama", nama);
                intent.putExtra("email", email);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MyView extends RecyclerView.ViewHolder {
        TextView nama, email;
        LinearLayout layout;
        public MyView(@NonNull View itemView) {
            super(itemView);
            nama = itemView.findViewById(R.id.nama);
            email = itemView.findViewById(R.id.email);
            layout = itemView.findViewById(R.id.layout);
        }
    }
}
