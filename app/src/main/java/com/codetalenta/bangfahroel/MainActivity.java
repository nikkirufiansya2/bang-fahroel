package com.codetalenta.bangfahroel;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity {
    List<MhsModel> mhsModels = new ArrayList<>();
    RecyclerView recyclerView;
    private static String url_select = Server.URL + "list-mhs.php";
    private static String url_select1 = Server.URL + "Mahasiswa.php";
    FloatingActionButton buttonAdd;
    String url_bang_fahroel = Server.URL_BANG_FAHROEL + "findallmahasiswa";
    Session session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        session = new Session(this);
        recyclerView = findViewById(R.id.recycler_view);
        buttonAdd = findViewById(R.id.floatingActionButton);
        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, InsertData.class));
            }
        });
        getDataApi();
    }

    /*
    * ini rest api nikki
    * */
    private void getData() {
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url_select, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONArray jsonArray = new JSONArray(response);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject data = jsonArray.getJSONObject(i);
                        mhsModels.add(new MhsModel(data.getInt("id"),data.getString("first_name"), data.getString("user_email")));
                    }
                    AdapterMhs adapterMhs = new AdapterMhs(MainActivity.this, mhsModels);
                    recyclerView.setLayoutManager(new LinearLayoutManager(MainActivity.this));
                    recyclerView.setAdapter(adapterMhs);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    /*
     * ini rest api nikki
     * */
    private void getData1() {
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url_select1, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("result");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject data = jsonArray.getJSONObject(i);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        AppController.getInstance().addToRequestQueue(stringRequest);
    }


    /*
     * ini rest api bang fahroel
     * */
    private void getDataApi(){
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url_bang_fahroel, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                JSONArray jsonArray = null;
                try {
                    jsonArray = new JSONArray(response);
                    mhsModels.clear();
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject data = jsonArray.getJSONObject(i);
                        mhsModels.add(new MhsModel(data.getInt("id"),data.getString("nama"), data.getString("email")));
                    }
                    AdapterMhs adapterMhs = new AdapterMhs(MainActivity.this, mhsModels);
                    recyclerView.setLayoutManager(new GridLayoutManager(MainActivity.this, 2));
                    recyclerView.setAdapter(adapterMhs);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                String token = session.getString("token");
                params.put("Authorization", token);
                return params;
            }

            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                if (response != null) {
                    System.out.println("Status Code : " + response.statusCode);
                }
                return super.parseNetworkResponse(response);
            }

        };

        AppController.getInstance().addToRequestQueue(stringRequest);
    }
}