package com.codetalenta.bangfahroel;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class EditData extends AppCompatActivity {
    EditText editTextName, editTextEmail;
    Button btnUpdate, btnDelete;
    String nama, email;
    Session session;
    int id;
    private String url = Server.URL_BANG_FAHROEL;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_data);
        editTextName = findViewById(R.id.name);
        editTextEmail = findViewById(R.id.email);
        session = new Session(this);
        getData();
        btnUpdate = findViewById(R.id.update);
        btnDelete = findViewById(R.id.delete);
        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                update();
                startActivity(new Intent(EditData.this, MainActivity.class));
                finish();
            }
        });
        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                delete();
                startActivity(new Intent(EditData.this, MainActivity.class));
                finish();
            }
        });
    }

    /*
    * method get data from AdapterMhs
    * untuk melihat data diambil cek di class AdapterMhs
    * */
    private void getData() {
        nama = getIntent().getStringExtra("nama");
        email = getIntent().getStringExtra("email");
        editTextName.setText(nama);
        editTextEmail.setText(email);
    }

    /*
    * method update data
    * */
    private void update() {
        id = getIntent().getIntExtra("id", 0);
        nama = editTextName.getText().toString().trim();
        email = editTextEmail.getText().toString().trim();
        JSONObject postData = new JSONObject();
        try {
            postData.put("id", id);
            postData.put("nama", nama);
            postData.put("email", email);
        } catch (JSONException e) {

        }
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.PUT, url + "updatemahasiswa/" + id, postData, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                String token = session.getString("token");
                params.put("Authorization", token);
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(jsonObjectRequest);
    }


    /*
    * method delete data
    * */
    private void delete() {
        id = getIntent().getIntExtra("id", 0);
        StringRequest stringRequest = new StringRequest(Request.Method.DELETE, url + "deletemahasiswa/" + id, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                String token = session.getString("token");
                params.put("Authorization", token);
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(stringRequest);
    }


}